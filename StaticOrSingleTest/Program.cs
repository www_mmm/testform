﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticOrSingleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var tzCollection = TimeZoneInfo.GetSystemTimeZones();
            foreach (TimeZoneInfo timeZone in tzCollection)
                Console.WriteLine("{0}", timeZone.Id);

            Console.WriteLine(St.GetFirst());
            Console.WriteLine(St.GetSecond());
            Console.ReadKey();
        }
    }


    public static class St
    {
        public static int i = 0 ;

        public static int GetFirst()
        {
            i++;
            return i;
        }

        public static int GetSecond()
        {
            i++;
            return i;
        }

    }

}
