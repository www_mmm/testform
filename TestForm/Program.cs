﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TestForm
{
    static class Program
    {        
        static void Main()
        {
            for (int i = 1; i <= 10; i++)
            {
               ThreadPool.QueueUserWorkItem(MyTask2,i);
               Thread.Sleep(200);
            }
                        
            Console.ReadKey();
        }


        static void MyTask()
        {
            int treadId = Thread.CurrentThread.ManagedThreadId;
            Console.WriteLine("Task started in theread {0}", treadId);
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(200);
                Console.WriteLine("+");
            }
            Console.WriteLine("Task finished in theread {0}", treadId);
        }

        static void MyTask1()
        {
            Console.WriteLine("My Task 1: Current Id {0} ManagedThreadId {1}", Task.CurrentId,Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(2000);
            Console.WriteLine("My Task 1: Current Id {0}", Task.CurrentId);
        }

        static void MyTask2(object i)
        {
            Console.WriteLine("My Task 1: ManagedThreadId {0} Param i = {1} ", Thread.CurrentThread.ManagedThreadId, i);
        }

    }

}
